score_differences = {}
score_differences["October 7, 2017"] = 8
score_differences["October 14, 2017"] = -12
score_differences["October 21, 2017"] = 3
# The missing code goes here but write it below. Assume that every game results in either a win or a loss.
losses = 0
wins = 0
for i in (score_differences.values()):
    if int(i) < 0:
        losses += 1
    else:
        wins += 1
        
print(wins, "wins -", losses, "losses") 
