import numpy as np
# -------------------------------
class Course:
    def __init__(self, rubric, number):
     self.rubric = rubric
     self.number = number
    def __str__(self):
     return self.rubric + " " + str(self.number)

class Course_Schedule:
    def __init__(self,course):
        self.array = np.array(["         ","       ","        "])

    def add(self,course):
        status = False
        i = 0
        while status == False:
            if self.array[i].strip() == "":
                self.array[i] = course
                status = True
            i += 1
    def __str__(self):
        return ("My Schedule\n" + "---------\n" + self.array[0] +"\n" + self.array[1]
                + "\n" + self.array[2])
            
# -------------------------------
def main():
    my_courses = Course_Schedule(3)
    course_1 = Course("CSCI", 127)
    my_courses.add(course_1)
    course_2 = Course("M", 171)
    my_courses.add(course_2)
    course_3 = Course("WRIT", 101)
    my_courses.add(course_3)
    print(my_courses)
# -------------------------------
main()
