

import matplotlib.pyplot as plt
years = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018] # bridger bowl year
total_snowfall = [253, 304, 388, 265, 283, 209, 194, 271, 177] # inches
largest_snowfall = [19, 16, 19, 25, 20, 14, 13, 20, 15] # inches
location = 0
location2 = 0
location = total_snowfall.index(max(total_snowfall))
location2 = largest_snowfall.index(max(largest_snowfall))
        
fig, ax = plt.subplots(2)

ax[0].plot(years,total_snowfall,color = "blue")
ax[0].plot(years[location],max(total_snowfall),color = "yellow",marker = "o")
ax[0].set_ylabel("Total Snowfall")

ax[1].plot(years,largest_snowfall,color = "blue")
ax[1].plot(years[location2],max(largest_snowfall),color = "yellow",marker = 'o')
ax[1].set_ylabel("Largest Snowfall")
ax[1].set_xlabel("Years")
plt.show()

#


