def divisors(number):
    list_of_divisors = []
    for i in range(1,number+1):
        if number % i == 0:
            list_of_divisors.append(i)
    return list_of_divisors

print(divisors(9))
print(divisors(7))
print(divisors(24))
