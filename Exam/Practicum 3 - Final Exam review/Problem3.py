import numpy as np # Do not import anything else
import matplotlib.pyplot as plt
units = ["CS", "ChBE", "Civil", "M&IE", "General", "CpE"] # See question 1 for a description
enrollments = [552, 563, 731, 1463, 210, 410] # See question 1 for a description
color_s = ['Blue','Gold']
wedge = {"linewidth" : 1.8,"edgecolor": "w"}
plt.pie(enrollments,labels = units,colors = color_s*3,counterclock = False,wedgeprops = wedge)
plt.show()


# Write the missing statements below this comment
