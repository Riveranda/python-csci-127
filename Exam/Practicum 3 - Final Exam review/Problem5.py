def divisors(number):
    list_of_divisors = []
    for i in range(1,number+1):
        if number % i == 0:
            list_of_divisors.append(i)
    return list_of_divisors

print(divisors(9))
print(divisors(7))
print(divisors(24))
print("")

def is_prime(num):
    return len(divisors(num)) == 2

print(is_prime(2))
print(is_prime(10))
print(is_prime(14033))
