class Rectangle:

    def __init__(self,x,y):
        self.x = x
        self.y = y
        
    def get_area(self):
        return self.x * self.y
    
    def is_square(self):
        if self.x == self.y:
            return True
        return False
    
    def __str__(self):
        return(str(str(self.x)+"x"+str(self.y)))
        
    
    


rect1 = Rectangle(4,4)
rect2 = Rectangle(4,5)
rect3 = Rectangle(8,1)
print(rect1)
print(rect2.get_area())
print(rect3.is_square())
print(rect1.is_square())
