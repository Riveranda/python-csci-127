class Date:
    def __init__(self, month, day, year):
        self.month = month
        self.day = day
        self.year = year

    def __str__(self):
        return str(self.month) + "/" + str(self.day) + "/" + str(self.year)
# your code here, but write it below

class Game:
    def __init__(self, school_1, school1_score, school_2, school2_score, date):
        if school1_score > school2_score:
            self.winner == school_1
            self.looser == school_2
            self.winscore  == school1_score
            self.lscore == school2_score
        else:
            self.winner == school_2
            self.looser == school_1
            self.winscore  == school2_score
            self.lscore == school1_score
        self.date = date
    def get_date(self):
        return self.date

    def __str__(self):
        print(self.winner + " "
        
championship = Game("Montana State", 62, "Idaho State", 56, Date(3, 11, 2017))
print(championship)

ncaa = Game("Montana State", 63, "Washington", 91, Date(3, 18, 2018))
print(ncaa)

##should yield
##
##Montana State beat Idaho State on 3/11/2017: 62 to 56
##Washington beat Montana State on 3/18/2018: 91 to 6
