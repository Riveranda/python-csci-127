class Rectangle:

    def __init__(self,x,y):
        self.x = x
        self.y = y
        
    def get_area(self):
        return self.x * self.y
    
    def is_square(self):
        if self.x == self.y:
            return True
        return False
    
    def __str__(self):
        return(str(str(self.x)+"x"+str(self.y)))

    def add_top(self,rect):
        rect_string = str(rect)
        if int(self.x) == int(rect_string[0]):
            self.y = int(rect_string[2])+ int(self.y)
        else:
            print("That rectangle cannot be added on the top")

    def add_right(self,rect):
        rect_string = str(rect)
        if int(self.y) == int(rect_string[2]):
            self.x = int(rect_string[0]) + int(self.x)
        else:
            print("That rectangle cannot be added on the right")

rect1 = Rectangle(4,4)
rect2 = Rectangle(4,5)
rect3 = Rectangle(8,1)
rect4 = Rectangle(3,1)
rect1.add_top(rect2)
print(rect1)
rect3.add_right(rect4)
print(rect3)
rect3.add_right(rect1)
print(rect3)
rect3.add_top(rect1)


