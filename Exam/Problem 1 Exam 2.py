#exam 2 problem 1
class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def is_puppy(self):
        if self.age <= 1:
            return True
    def __str__(self):
        return("Dog named {}".format(self.name))


bruce = Dog("Bruce", 1)
jada = Dog("Jada", 12)
luna = Dog("Luna", 3)

dogs = [bruce, jada, luna]
for dog in dogs:
    if not dog.is_puppy():
        print(dog)


#dog named Jada
#dog named Luna
