# --------------------------------------
# CSCI 127, Lab 3                      |
# June 13, 2019                        |
# Timothy Bender                       |
# --------------------------------------
# Calculate how many z's are in a      |
# sentence using three techniques.     |
# --------------------------------------

def count_built_in(sentence):
    string = "z"
    count = sentence.count(string)
    return count

def count_iterative(sentence):
    count = 0
    for ch in sentence:
        if ch == "z":
            count = count + 1
    return count

def count_recursive(sentence):

    if sentence == "z":
        return 1
    elif sentence != "z" and len(sentence) == 1:
        return 0
    return count_recursive(sentence[0]) + count_recursive(sentence[1:])





# --------------------------------------

def main():
    answer = "yes"
    while (answer == "yes") or (answer == "y"):
        sentence = input("Please enter a sentence: ")
        sentence = sentence.lower()
        print()
        print("Calculating the number of z's using ...")
        print("---------------------------------------")
        print("Built-in function =", count_built_in(sentence))
        print("Iteration =", count_iterative(sentence))
        print("Recursion =", count_recursive(sentence))
        print()
        answer = input("Would you like to continue: ").lower()
        print()

# --------------------------------------

main()
