#----------------------------------|
# CSCI 127, Joy and Beauty of Data |
# Program 1: GPA Calculator        | 
# Timothy Bender                   |
# Last Edited: June 12, 2019       |
#----------------------------------|
# Create a program to calculate a  |
# user's gpa, given the number  |
# of courses, grade received and   |
# credit hours of the course.      |
#----------------------------------|

#translate will take an inputed string, and return the float value associated with that letter grade.
#to ensure that capital and lowercase inputs work, the input it lowered.
def translate(grade): 
    gradeearned = 0.0
    if grade == "a":    
       gradeearned = 4.00       
    elif grade == "a+": 
        gradeearned = 4.00
    elif grade == "a-":
        gradeearned = 3.70
    elif grade == "b+":
        gradeearned = 3.30
    elif grade == "b":
        gradearned = 3.00
    elif grade == "b-":
        gradeearned = 2.70
    elif grade == "c+":
        gradeearned = 2.30
    elif grade == "c":
        gradeearned = 2.00
    elif grade == "c-":
        gradeearned = 1.70
    elif grade == "d+":
        gradeearned = 1.30
    elif grade == "d":
        gradeearned = 1.00
    elif grade == "f":
        gradeearned = 0.00
    return gradeearned 
 
def main():
    numcl = int(input("Enter the number of courses you are taking: "))
    print (" ")

    def calculator(numcl):
        denominator = 0.0    
        numerator = 0.0
        for i in range(numcl):           
            grade = float(translate(input("Enter grade for course " + str((i+1)) + ": ").lower()))
            creditclass = float(input("Enter credits for course " + str((i+1)) + ": "))

            print (" ")

            numerator = numerator + (grade * creditclass)
            denominator = denominator + creditclass
        return(numerator / denominator)
    gpa = (calculator(numcl))
    print("Your GPA is " + '{:01.2f}'.format(gpa))

           
main()
