import math
import random 

def square_roots():
    print("")
    i = True
    while i == True:
        square = random.randint(10,250)
        ans = float(square**(1/2))
        print("Your number is {}.".format(square))
        user_guess = float(input("Please input the square root: "))
        print("The correct answer was {} your answer was {}.\n".format(ans,user_guess))
        user_input = (input("Continue practicing? (Y,N)\n").lower())
        if user_input == "n":
            i = False

def cube_roots():
    i = True
    streak = 0
    while i == True:
        ans = random.randint(10,100)
        cube = ans**3
        print("\nYour number is {}.".format(cube))
        user_guess =int(input("Please input the cube root: "))
        if user_guess == ans:
            streak += 1
        else:
            streak = 0 
        print("The correct answer was {} your answer was {:,.2f}.\n".format(ans,user_guess))
        print("Streak of {} correct in a row.".format(streak))
        user_input = (input("Continue? N for no: ")).lower()
        if user_input == "n":
            i = False
            
def cube_roots_threedig():
    i = True
    streak = 0
    while i == True:
        ans = random.randint(100,999)
        cube = ans**3
        print("\nYour number is {:,.2f}".format(cube))
        print(cube)
        user_guess =int(input("Please input the cube root: "))
        if user_guess == ans:
            streak += 1
        else:
            streak = 0 
        print("The correct answer was {} your answer was {}.\n".format(ans,user_guess))
        print("Streak of {} correct in a row.".format(streak))
        user_input = (input("Continue? N for no: ")).lower()
        if user_input == "n":
            i = False
    
            
def two_digit_multiplication():
    i = True
    streak = 0
    while i == True:
        rand1 = random.randint(10,99)
        rand2 = random.randint(10,99)
        ans = rand1*rand2
        print("\nPlease multiply {}\n               x{}\n               -----".format(rand1,rand2))
        user_guess =int(input("Please input answer: "))
        if user_guess == ans:
            streak += 1
        else:
            streak = 0 
        print("The correct answer was {} your answer was {}.\n".format(ans,user_guess))
        print("Streak of {} correct in a row.".format(streak))
        user_input = (input("Continue? N for no: ")).lower()
        if user_input == "n":
            i = False
    
def main():
    i = True
    while i == True:
        print("Enter 1 for square root practice.\nEnter 2 for cube root practice.\nEnter 3 for Three digit cubes.\nEnter 4 for two digit multiplication.\nPress 5 to exit.")
        user_choice = int(input("Please enter your selection: "))
        if user_choice == 1:
            square_roots()
        elif user_choice == 2:
            cube_roots()
        elif user_choice == 3:
            cube_roots_threedig()
        elif user_choice == 4:
            two_digit_multiplication()
        elif user_choice == 5:
            i = False
        else:
            print("Not a valid entry")

main()
