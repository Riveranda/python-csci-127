# -----------------------------------------------------
# CSCI 127, Lab 7
# June 24, 2019
# Timothy Bender
# -----------------------------------------------------

class Contact:
    
    def __init__ (self, firstName, lastName, cellNumber):
            self.first_name = firstName
            self.last_name = lastName
            self.cell_number = cellNumber
            self.title = ""

    def get_cell_number(self):
        return self.cell_number

    def get_area_code(self):
        return (self.cell_number)[0:3]

    def set_first_name(self,name):
        self.first_name = name

    def set_title(self,title):
        self.title = title
        
    def print_entry(self):
        print 
        print((self.title + " " + self.first_name + " " + self.last_name).lstrip() + (25 - len((self.title + " " + self.first_name + " " + self.last_name).lstrip()))*" " + self.cell_number)
                
        
        
def print_directory(contacts):
    print("My Contacts")
    print("-----------")
    for person in contacts:
        person.print_entry()
    print("-----------\n")

# -----------------------------------------------------

def main():
    champ = Contact("???", "Bobcat", "406-994-0000")
    president = Contact("Waded", "Cruzado", "406-994-CATS")
    professor = Contact("John", "Paxton", "406-994-4780")

    contacts = [champ, president, professor]

    print_directory(contacts)

    champ.set_first_name("Champ")
    president.set_title("President")
    professor.set_title("Professor")

    print_directory(contacts)

    print("The area code for cell number", champ.get_cell_number(), "is", \
           champ.get_area_code())

# -----------------------------------------------------

main()
