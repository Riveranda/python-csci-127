import matplotlib.pyplot as plt
import pandas as pd
import os

# -------------------------------------------------
# CSCI 127, Lab 11                                |
# July 2, 2019                                    |
# Timothy Bender                                  |
# -------------------------------------------------

"""Age-histogram.png shows some interesting figures. To begin there is a massive spike in the number
of billionars between the ages of 0 and ~10. Afterwards the data is what you would expect. The number of billionars
flatlines throughout the teenage years, then gradually increases to a peak around 70. This is fairly logical as people tend
to accule more wealth as they age."""


def get_data_shape(data):
    return data.shape[0],data.shape[1]
    
def compute_total_wealth(data):
    return data.groupby('year')['worth in billions'].sum()

def compute_num_women(data):
    return data.gender.value_counts()['female']

def plot_histogram(data, file_name):
    plt.hist(data.age)
    plt.savefig(file_name)

##### do not change anything below this line

def main():

    print("This program calculates some information from billionaires.csv.\n")

    data = pd.read_csv('billionaires.csv')
    rows, columns = get_data_shape(data)
    total_wealth_by_year = compute_total_wealth(data)
    num_women = compute_num_women(data)
    plot_histogram(data, "age_histogram.png")

    print("The data set has {} rows and {} columns.".format(rows, columns))
    print("\nThe total wealth of billionaires by year was:")
    print(total_wealth_by_year)
    print("\nThe number of female billionaires in the data set is {}.".format(num_women))

main()
