# -----------------------------------------+
# CSCI 127, Joy and Beauty of Data         |
# Program 3: Weather CSV Library           |
# Timothy Bender                           |
# Last Modified: June 20, 2019             |
# -----------------------------------------+
#This program will scan through weather.csv|
#And use this data to determine the coldest|
#temperature, average temperature based on |
#user provided location, and list weather  |
#stations by user provided state. The User |
#will have full control over which function|
#is called, and will not terminate until 5 |
#is inputed. There is also a snowy days    |
#calculated                                |
# -----------------------------------------+

#Header for weather.vsc
#Average,City,Code,Direction,Full,Location,Maximum,Minimum,Month,Precipitation,Speed,State,Week of,Year

def coldest_temperature(f):
    f = open(f, "r")
    f.readline()
    coldest = 50
    city = ""
    state = ""
    date = ""
    for i in f:
        values = i.split(",")
        if int(str(values[8])) < int(coldest):
             coldest = values[8]
             city = values[5]
             state = values[6]
             date = values[4]
    #strip extra " from city and state
    city = city[1:]
    state = state[:-1]
    print("Coldest Fahrenheit temperature reading: {}".format(coldest))
    print("Location: " + str(city + " " + state))
    print("Date: " + date)
    f.close()

def average_temperature(f,loc):
    f = open(f,"r")
    f.readline()
    total_readings = 0
    total_for_average = 0
    average = 0
    location = []
    
    for i in f:
        location = []
        values = i.split(",")
        #In order to do this i had to stitch two elements of a list together into a single list
        #manipulating values and location to be be in the same string format for comparison later
        #all other methods threw off weird errors so i'm leaving this one as is 
        location.append((str(values[5][1:]+ "," + values[6][:-1])).lower())
        location.append(str(loc).lower())

        if location[0] == location[1]:
            total_readings = total_readings + 1
            total_for_average += int(values[0])
            average = total_for_average / total_readings
    print("Number of readings: {}".format(total_readings))
    if total_readings == 0:
        print("Average temperature: Not Applicable")
    else:
        print("Average temperature: {:0.2f}".format(average))
    f.close()

def all_stations_by_state(f, state):
    #pos 12 for state pos 1 for city
    f = open(f, "r")
    f.readline()
    list_of_stations = []
    for i in f:
        values = i.split(",")
        if (str(values[12]).lower()) == state.lower() and list_of_stations.count((values[1].lower())) == 0:
             list_of_stations.append((values[1].lower()))
    count_up = 0
    if len(list_of_stations) == 0:
        print("There are no recording stations")

    else:
        list_of_stations.sort()
        print("Recording Stations")
        print("------------------")
        for i in list_of_stations:
            count_up += 1
            if count_up < 10 :
                print(" " + str(count_up)+ ". " + i.title())
            else:
                print("" + str(count_up)+ ". " + i.title())
    f.close()


#Let_it_snow will report the days which a certain city could have recieved snow. If precipitation > .04 inches and the low temperature
#of the day was 32 degrees or below, it will be reported as a possibility.
#fyi miles city works well to test it. Anchorage works great too.
#anything less than .04 inches can barely be considered precipitation so... to keep the number of possibilities reasonable i omitted
#anything below this threshold
    
def let_it_snow(f,loc):
    f = open(f, "r")
    f.readline()
    list_of_snowy_days = []
    list_of_precip = []
    list_of_temps = []
    for i in f:
        values = i.split(",")
        if ((str(values[1]).lower()) == (str(loc).lower())) and (float(values[10]) > .04) and (float(values[8]) <= 32):
        #9 and 7
            list_of_snowy_days.append(values[4])
            list_of_precip.append(values[10])
            list_of_temps.append(values[8])
            
    print(" ")
    
    if len(list_of_snowy_days) == 0:
        print("There were no possible snowy days in " + loc.title())
        print(" ")
    else:
        print("Snowy Days in " + loc.title() + ":")
        for i in range(len(list_of_snowy_days)):
            print("On " + str(list_of_snowy_days[i]) + " " + loc.title() + " recieved " + str(list_of_precip[i]) + " inches of precipitation,")
            print("and had a low temperature of " + str(list_of_temps[i]) +"°F. You likely received snow!")
            print(" ")
        print("Do you wanna build a snowman?")
        print("Come on, lets go and play!")


    f.close()

# -----------------------------------------+
# Do not change anything below this line   |
# with the exception of code related to    |
# option 4.                                |
# -----------------------------------------+

# -----------------------------------------+
# menu                                     |
# -----------------------------------------+
# Prints a menu of options for the user.   |
# -----------------------------------------+

def menu():
    print()
    print("1. Identify coldest temperature.")
    print("2. Identify average temperature for a given location.")
    print("3. Identify all recording station locations by state.")
    print("4. Identify snowy days by location.")
    print("5. Quit.")
    print()

# -----------------------------------------+
# main                                     |
# -----------------------------------------+
# Repeatedly query the user for options.   |
# -----------------------------------------+

def main():
    input_file = "weather.csv"
    choice = 0
    while (choice != 5):
        menu()
        choice = int(input("Enter your choice: "))
        print()
        if (choice == 1):
            coldest_temperature(input_file)
        elif (choice == 2):
            location = input("Enter desired location (e.g. Miles City, MT): ")
            average_temperature(input_file, location)
        elif (choice == 3):
            state = input("Enter name of state (e.g. Montana): ")
            all_stations_by_state(input_file, state)
        elif (choice == 4):
            location = input("Enter desired city (e.g. Miles City): ")
            let_it_snow(input_file,(location).lower())
        elif (choice != 5):
            print("That is not a valid option.  Please try again.")
    print("Goodbye!")

# -----------------------------------------+

main()
