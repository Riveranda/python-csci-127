# -----------------------------------------+
# Timothy Bender                           |
# CSCI 127, Program 2                      |
# Last Updated: June 17, 2019              |
# -----------------------------------------|
# A simplified Cribbage scoring system.    |
# -----------------------------------------+

def flush(suits):
    if suits[0] == suits[1] == suits[2] == suits[3] == suits[4]:
        return 5
    else:
        return 0 

def pair(ranks):
    scorereturn = 0
    for i in range(len(ranks)):
        for j in range(i+1, 5):
            if ranks[i] == ranks[j]:
                scorereturn = scorereturn + 2 
    return scorereturn 
        
def change_cards(rank):
    if rank == "Two":
        return 2
    elif rank == "Three":
        return 3 
    elif rank == "Four":
        return 4
    elif rank == "Five":
        return 5
    elif rank == "Six":
        return 6
    elif rank == "Seven":
        return 7
    elif rank == "Eight":
        return 8
    elif rank == "Nine":
        return 9
    elif rank == "Ten":
        return 10
    elif rank == "Jack":
        return 10
    elif rank == "Queen":
        return 10
    elif rank == "King":
        return 10
    elif rank == "Ace":
        return 11
    
    
def fifteens(ranks):
    scorereturn = 0
    for i in range(len(ranks)):
        for j in range(i+1, 5):
            if int(change_cards(ranks[i]))+ int(change_cards(ranks[j])) == 15:
                scorereturn = scorereturn + 2 
    return scorereturn




def print_hand(hand_as_list, number):
    hand = hand_as_list[:]
    print("Hand " + str(number)+ ":" ,end=" ")
    for i in range(5):
        toprint = (hand[0][0] + " " + hand[0][1] +",")
        hand = hand[1:]
        if i == 4:
            toprint = toprint[0:-2]
        print(toprint ,end=" ")
    print(" ")

def evaluate_hand(hand_as_list):
    #write program that separates hand_as_list into two lists
    suits = [hand_as_list[0][1],hand_as_list[1][1],hand_as_list[2][1], hand_as_list[3][1], hand_as_list[4][1]]
    ranks = [hand_as_list[0][0],hand_as_list[1][0],hand_as_list[2][0],hand_as_list[3][0],hand_as_list[4][0]]
    #calculate total score
    score = int(flush(suits)) + int(pair(ranks)) + int(fifteens(ranks))
    #print the score
    print("Points scored: " + str(score))
    print(" ")

# -----------------------------------------+
# Do not change anything below this line.  |
# -----------------------------------------+

def process_hands(cribbage_input, cards_in_hand):
    number = 1
    for hand in cribbage_input:
        hand = hand.split()
        hand_as_list = []
        for i in range(cards_in_hand):
            hand_as_list.append([hand[0].capitalize(), hand[1].capitalize()])
            hand = hand[2:]
        print_hand(hand_as_list, number)
        evaluate_hand(hand_as_list)
        number += 1

# -----------------------------------------+

def main():
    cribbage_file= open("cribbage.txt", "r")
    process_hands(cribbage_file, 5)
    cribbage_file.close()

# -----------------------------------------+

main()
