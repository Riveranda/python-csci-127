import numpy as np
import matplotlib.pyplot as plt

# -------------------------------------------------
# CSCI 127, Lab 10                                |
# July 1, 2019                                    |
# Timothy Bender                                  |
# -------------------------------------------------

def read_file(file_name):
    f = open(file_name, "r")
    f.readline()
    
    names = []
    enrollment = []
    name_array = np.array(["       ","","","","","","",])
    enrollment_array = np.zeros(7)
    count = 0 
    for i in f:
        values = i.split(",")
        names.append(values[0])
        enrollment.append(values[1][:-1])
    for i in names:
        name_array[count] = i
        count += 1
    count = 0 
    for i in enrollment:
        enrollment_array[count] = i
        count += 1
    return name_array, enrollment_array
        
        

# -------------------------------------------------

def main(file_name):
    college_names, college_enrollments = read_file(file_name)
    x_pos = [800,1600,2400,3200,4000,4800,5600]
    colors = np.array(["blue","gold","blue","gold","blue","gold","blue"])

    fig, ax = plt.subplots()
    ax.bar(x_pos,college_enrollments,650, tick_label = college_names, color = colors)
    ax.set_title("Montana State University Fall 2018 Enrollments")
    ax.set_xlabel("College Name")
    ax.set_ylabel("College Enrollment")
    ax.set_yticks(np.arange(0,4800,400))
    plt.show()
    

# -------------------------------------------------

main("fall-2018.csv")
