# --------------------------------------
# CSCI 127, Lab 7                      |
# June 20, 2019                        |
# Timothy Bender                       |
# --------------------------------------
#the comma and space character are treated differently because the dictionary
#key created from the ascii file is is "comma" for comma and for space it is "space".
#The other keys are simply their characters
def create_dictionary(f):
    f = open(f, "r")
    encodings = {}
    for i in f:
        values = i.split()
        for t in values:
            values_2 = t.split(",")
            if encodings.get(values_2[1],-1) == -1:
                encodings[values_2[1]] = values_2[0]
    f.close()
    return encodings

def translate(sen, encodings, t):
    f = open(t, "w")
    for i in sen:
        if i == " ":
            f.write(i + " " + encodings["space"] + "\n")
        elif i == ",":
            f.write(i + " " + encodings["comma"] + "\n")
        elif encodings.get(i,-1) == -1:
            f.write(i +" UNKNOWN \n")
        else:
            f.write(i + " " + encodings[i] + "\n")
             
    f.close()
# --------------------------------------

def main():
    dictionary = create_dictionary("ascii-codes.csv")
    sentence = "Buck lived at a big house in the sun-kissed Santa Clara Valley. Judge Miller's place, it was called!"
    translate(sentence, dictionary, "output-1.txt")
    sentence = "Bozeman, MT  59717"
    translate(sentence, dictionary, "output-2.txt")
    sentence = "The value is ~$25.00"
    translate(sentence, dictionary, "output-3.txt")

# --------------------------------------

main()
