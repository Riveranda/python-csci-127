import numpy as np

# ---------------------------------------
# Timothy Bender
# Program 5: Eight Puzzle
# Last Edited: July 1, 2019
# ---------------------------------------
# This program is an EightPuzzel game. The first puzzel will be the solution, and thus
# will return a correct answer with zero moves. The next puzzel will require user input
# ---------------------------------------

class EightPuzzle:

    def __init__(self):
        self.solution = np.array([1,2,3,4,5,6,7,8," "])
        self.solution = self.solution.reshape(3,3)

    def __str__(self):
        separator = "+-+-+-+\n"
        answer = separator
        for row in range(3):
            for col in range(3):
                answer += "|" + str(self.puzzle[row][col])
            answer += "|\n"
            answer += separator
        return answer

    def puzzle_1(self):
        self.puzzle = np.array([1,2,3,4,5,6,7,8," "])
        self.puzzle = self.puzzle.reshape(3,3)
        self.blank_x = 2
        self.blank_y = 2

    def puzzle_2(self):
        self.puzzle = np.array([4,1,3,7,2,5,8," ", 6])
        self.puzzle = self.puzzle.reshape(3,3)
        self.blank_x = 2
        self.blank_y = 1

    def swap_positions(self, x1, y1, x2, y2):
        self.puzzle[x1][y1], self.puzzle[x2][y2] = \
                             self.puzzle[x2][y2], self.puzzle[x1][y1]

# ---------------------------------------
# Do not change anything above this line
# ---------------------------------------

    def is_puzzle_solved(self):
        if (np.array_equal(self.puzzle,self.solution)) == True:
            return True
        
        

    def move_blank(self):
        #finding the location of the blank space and assigning it's position to an array
        storage = np.where(self.puzzle == " ")
        location_of_blank = np.array([storage[0][0],storage[1][0]])
        move_data = {"up":-1, "down":1,"left":-1,"right":1}
        loop_var = False
        #creating a while loop which will keep the function from terminating if an invalid move is entered
        while loop_var == False: 
            move = input("Enter choice [up, down, left, right]: ")
            move = move.lower()
            #Protection against illegal entries, all legal entries will be in move_data.keys(), so if its not in there, its illegal.
            if (move in move_data.keys()) == False:
                print("That move is invalid. Please try again.")
#Separate the logic into up and down, and left and right. This simplifies finding the new coordinates.
            if move == "up" or move == "down":
                if location_of_blank[0] + move_data[move] < 0 or location_of_blank[0] + move_data[move] > 2:
#The gamespace is a 3 by 3 square. Therefore we cannot have any positions that contain values less than zero or greater than 2.            
                    print("That move is invalid. Please try again.")
                else:
                    loop_var = True
                    self.swap_positions(location_of_blank[0],location_of_blank[1], location_of_blank[0] + move_data[move], location_of_blank[1])
                    
            if move == "left" or move == "right":
                if location_of_blank[1] + move_data[move] < 0 or location_of_blank[1] + move_data[move] > 2:
                    print("That move is invalid. Please try again.")
                else:
                    loop_var = True
                    self.swap_positions(location_of_blank[0],location_of_blank[1], location_of_blank[0], location_of_blank[1] + move_data[move])

# ---------------------------------------
# Do not change anything below this line
# ---------------------------------------

def solve(puzzle):
    steps = 0
    print("Puzzle:\n")
    print(puzzle)
    while not puzzle.is_puzzle_solved():
        puzzle.move_blank()
        print(puzzle)
        steps += 1
    print("Congratulations - you solved the puzzle in", steps, "steps!\n")


def main():
    puzzle = EightPuzzle()
    puzzle.puzzle_1()
    solve(puzzle)
    puzzle.puzzle_2()
    solve(puzzle)

# ---------------------------------------

main()
