# --------------------------------------
# CSCI 127, Lab 4
# February 17, 2019
# Timothy Bender
# --------------------------------------

def process_season(season, games_played, points_earned):
    print("Season: " + str(season) + ", Games Played: " + str(games_played) +
          ", Points earned: " + str(points_earned))
    print("Possible Win-Tie-Loss Records")
    print("-----------------------------")
    wins = int(points_earned/3)
    ties = points_earned - (wins*3)
    losses = games_played - wins - ties
    while wins >= 0 and ties >= 0 and losses >= 0:
        print (str(wins) + "-" + str(ties) + "-" + str(losses))
        wins = wins - 1
        ties = ties + 3
        losses = games_played - wins - ties


            
        

# --------------------------------------

def process_seasons(seasons):
    for i in range(len(seasons)):
        process_season(i+1, seasons[0+i][0], seasons[0+i][1])

# --------------------------------------

def main():
    # format of list: [[season-1-games, season-1-points], [season-2-games, season-2-points], etc.]
    soccer_seasons = [[1, 3], [1, 1], [1, 0], [20, 30]]
    test_1 = [[2,3], [1,1], [6,0], [20,30]]
    
    process_seasons(soccer_seasons)

# --------------------------------------

main()
