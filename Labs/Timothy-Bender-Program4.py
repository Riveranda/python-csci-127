import string

# ---------------------------------------
# CSCI 127, Joy and Beauty of Data      |
# Program 4: Pokedex                    |
# Timothy Bender                        |
# Last Modified: June 26, 2019          |
# ---------------------------------------
# This program will import the pokedex file, sort it into a list and then perform different operations using that data.
# Object orientation will be used to print the pokedex, to allow the user to lookup a pokemon's stats based on name and number
# Print out the total of pokemon that contain a user defined type, and finally print out the mean average combat points for all pokemon
# Contained in pokedex
# ---------------------------------------

class Pokemon:

    def __init__(self, name, number, combat_points, types):
        self.name = name
        self.number = number
        self.combat_points = combat_points
        self.types = types

    def __str__(self):
        list_of_types = ""
        for i in range(len(self.types)):
            list_of_types += (self.types[i] + " and ")
        return("Number: " + str(self.number) +", Name: " + self.name.capitalize() + ", CP: " + str(self.combat_points) + ": Type: " + list_of_types[0:-5])

    def get_name(self):
        return self.name

    def get_number(self):
        return self.number

    def get_combat_points(self):
        return self.combat_points

    def get_type(self):
        return self.types
        

    
def print_menu():
    print("1. Print Pokedex")
    print("2. Print Pokemon by Name")
    print("3. Print Pokemon by Number")
    print("4. Count Pokemon with Type")
    print("5. Print Average Pokemon Combat Points")
    print("6. Quit")
    print("")

def print_pokedex(pokedex):
    print("The Pokedex")
    print("-----------")
    for i in pokedex:
        print(i)

def lookup_by_name(pokedex, name):
    x = 0
    name.lower()
    for i in pokedex:
        if i.get_name() == name:
            print(i)
        else:
            x += 1
    if len(pokedex) == x:
        print("There is no Pokemon named " + name)

def lookup_by_number(pokedex, number):
    x = 0
    for i in pokedex:
        if i.get_number() == number:
            print(i)
        else:
            x += 1
    if len(pokedex) == x:
        print("There is no Pokemon number " + str(number))

def total_by_type(pokedex, pokemon_type):
    count = 0
    pokemon_type.lower()
    for i in pokedex:
        if (pokemon_type in (i.get_type())) == True:
            count += 1
    print("Number of Pokemon that contain type " + pokemon_type +" = " + str(count))

def average_hit_points(pokedex):
    total_top = 0
    for i in pokedex:
        total_top += i.get_combat_points()
    print("Average Pokemon combat points = {:0.2f}".format(total_top/len(pokedex)))


# ---------------------------------------
# Do not change anything below this line
# ---------------------------------------

def create_pokedex(filename):
    pokedex = []
    file = open(filename, "r")
    
    for pokemon in file:
        pokelist = pokemon.strip().split(",")
        number = int(pokelist[0])               # number
        name = pokelist[1]                      # name
        combat_points = int(pokelist[2])        # hit points
        types = []
        for position in range(3, len(pokelist)):
            types += [pokelist[position]]       # type
        pokedex += [Pokemon(name, number, combat_points, types)]
    file.close()
    return pokedex

# ---------------------------------------

def get_choice(low, high, message):
    legal_choice = False
    while not legal_choice:
        legal_choice = True
        answer = input(message)
        for character in answer:
            if character not in string.digits:
                legal_choice = False
                print("That is not a number, try again.")
                break 
        if legal_choice:
            answer = int(answer)
            if (answer < low) or (answer > high):
                legal_choice = False
                print("That is not a valid choice, try again.")
    return answer

# ---------------------------------------

def main():
    pokedex = create_pokedex("pokedex.txt")
    choice = 0
    while choice != 6:
        print_menu()
        choice = get_choice(1, 6, "Enter a menu option: ")
        if choice == 1:
            print_pokedex(pokedex)
        elif choice == 2:
            name = input("Enter a Pokemon name: ").lower()
            lookup_by_name(pokedex, name)
        elif choice == 3:
            number = get_choice(1, 1000, "Enter a Pokemon number: ")
            lookup_by_number(pokedex, number)
        elif choice == 4:
            pokemon_type = input("Enter a Pokemon type: ").lower()
            total_by_type(pokedex, pokemon_type)
        elif choice == 5:
            average_hit_points(pokedex)
        elif choice == 6:
            print("Thank you.  Goodbye!")
        print()

# ---------------------------------------

main()
