# --------------------------------------
# CSCI 127, Lab 6                      |
# June 19, 2019                        |
# Timothy Bender                       |
# --------------------------------------

#I used the updated version of earthquakes.csv

def average_magnitude(file):
    f = open(file, "r")
    f.readline()
    total = 0
    total_earthquakes = 0 
    for line in f:
        values = line.split(",")
        total += float(values[8])
        total_earthquakes += 1
    f.close()
    return(total/total_earthquakes)
        

def count_earthquakes(file,lb,ub):
    f = open(file, "r")
    f.readline()
    total = 0
    for line in f:
        values = line.split(",")
        if float(values[8]) >= lb and float(values[8]) <= ub:
            total += 1
    f.close()
    return total

def earthquake_locations(file):
    f = open(file,"r")
    f.readline()
    locations = []
    string = "" 
    for line in f:
        values = line.split(",")
        if locations.count(values[11]) == 0:
            locations.append(values[11])
    locations.sort()
    for i in range(len(locations)):
        if (i+1) < 10:
            print ("  " + str(i+1) + ". " + str(locations[i])) 
        elif (i+1) >= 10 and (i+1) < 100:
            print(" " +str(i+1) + ". " + str(locations[i]))
        else:
            print(str(i+1) + ". " + str(locations[i]))
    print(" ")
    f.close()
    

    


# --------------------------------------

def main(file_name):
    magnitude = average_magnitude(file_name)
    print("The average earthquake magnitude is {:.2f}\n".format(magnitude))


    print("Alphabetical Order of Earthquake Locations")
    print("------------------------------------------")
    earthquake_locations(file_name)
##
    lower_bound = float(input("Enter a lower bound for the magnitude: "))
    upper_bound = float(input("Enter an upper bound for the magnitude: "))
    how_many = count_earthquakes(file_name, lower_bound, upper_bound)
    print("Number of recorded earthquakes between {:.2f} and {:.2f} = {:d}".format(lower_bound, upper_bound, how_many))

# --------------------------------------

main("earthquakes.csv")
