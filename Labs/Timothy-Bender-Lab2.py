# ---------------------------------------
# CSCI 127, Joy and Beauty of Data      |
# Lab 2: Tax Calculator                 |
# Timothy Bender                        |
# Date: June 11, 2019                   |
# ---------------------------------------
# Calculate the amount of tax owed by an|
# unmarried taxpayer in tax year 2018.  |
# ---------------------------------------

def unmarried_individual_tax(x):
    tax_owed = 0.0 #define our variable tax_owed
    if x < 9700 and x >= 0:
        tax_owed = tax_owed + x * .1

    elif x >= 9700 and x < 39475:
        tax_owed = tax_owed +(9700*.1)+(x-9700)*.12 

    elif x >= 39475 and x < 84200:
        tax_owed = tax_owed +(9700*.1)+(39475-9700)*.12 + (x - 39475)*.22

    elif x >= 84200 and x < 160725:
        tax_owed = tax_owed +(9700*.1)+(39475-9700)*.12 + (84200 - 39475)*.22 + (x - 84200)*.24

    elif x >= 160725 and x < 204100:
        tax_owed = tax_owed +(9700*.1)+(39475-9700)*.12 + (84200 - 39475)*.22 + (160725 - 84200)*.24 +(x -160725)*.32

    elif x >= 204100 and x < 510300:
        tax_owed = tax_owed +(9700*.1)+(39475-9700)*.12 + (84200 - 39475)*.22 + (160725 - 84200)*.24 +(204100 -160725)*.32 + (x - 204100)*.35

    elif x >= 51300:
        tax_owed = tax_owed +(9700*.1)+(39475-9700)*.12 + (84200 - 39475)*.22 + (160725 - 84200)*.24 +(204100 -160725)*.32 + (510300 - 204100)*.35 + (x - 510300)*.37
    return tax_owed


def process(income):
    print("The 2018 taxable income is ${:.2f}".format(income))
    tax_owed = unmarried_individual_tax(income)
    print("An unmarried individual owes ${:.2f}\n".format(tax_owed))


#---------------------------------------

def main():

    process(5000)      # test case 1
    process(20000)     # test case 2
    process(50000)     # test case 3
    process(100000)    # test case 4
    process(200000)    # test case 5
    process(400000)    # test case 6
    process(600000)    # test case 7

# ---------------------------------------

main()
