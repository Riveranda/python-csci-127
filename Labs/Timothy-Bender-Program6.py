import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
#-------------------------------------
# CSCI 127 Joy and Beauty of Data
#-------------------------------------
# Timothy Bender
# July 2, 2019
#-------------------------------------
"""Monthly cancellations will create a line graph that plots the total cancellations per month and the 
 total cancellations due to weather per month on the same graph. Here we can see a sharp increase during the winter months
 which would be expected due to worsened weather during those months. We also see an increase in delays during the summer months.
 It is possible this is due to higher traffic of customers during the summer. 
 

 Next we will create an overlayed bar graph that displays the percentage of delays per the total number of flights for the top 12 most delayed airports.
 This would allow airports to view where they can
 improve their efficiency and see where most of their delays are occuring
 due to this data we can conclude that NAS causes by far the most delayed flights, followed by late aircraft
 There are three major airports however that suffer more late aircraft than NAS delays .
 """

def monthly_cancellations(data):
    #These dictionaries will be used to convert the months to numeric digits for sorting, and then back
    months = {"Jan":1, "Feb":2, "Mar":3, "Apr":4, "May":5, "Jun":6, "Jul":7, "Aug":8, "Sep":9, "Oct":10, "Nov":11, "Dec":12}
    months_reverse = {1:"Jan", 2:"Feb", 3:"Mar", 4:"Apr", 5:"May", 6:"Jun", 7:"Jul", 8:"Aug", 9:"Sep", 10:"Oct", 11:"Nov", 12:"Dec"}

    #Slicing the month names to be 3 letters long
    data['Month Name'] = data["Month Name"].str[:3]
    #Setting the month name column to numeric values so that .groupby will sort them into order
    data["Month Name"] = data["Month Name"].map(months)
    
    monthly_sort = data.groupby('Month Name').sum()['Cancelled']
    monthly_sort2 = data.groupby('Month Name').sum()['# of Delays.Weather']

    #resetting the months to original string names
    list_months = monthly_sort.index.map(months_reverse)
    monthly_sort.index = list_months
    monthly_sort2.index = list_months
    
    return monthly_sort,monthly_sort2


def tot_delays(data):
    delays = data.groupby("Code").sum()['# of Delays.National Aviation System'].nlargest(12)
    total_flights = data.groupby("Code").sum()["Flights.Total"][delays.index]
    delays2 = data.groupby("Code").sum()["# of Delays.Late Aircraft"][delays.index]
    delays3 = data.groupby("Code").sum()["# of Delays.Carrier"][delays.index]
    return delays,delays.index,total_flights,delays2,delays3
    

def get_percent(delays,total):    
    return (delays/total)*100

def main():
    data = pd.read_csv('airlines.csv')
    
    cancellations,weather_cancellations= monthly_cancellations(data)
    
    #first figure construction
    fig1, ax1 = plt.subplots()
    ax1.plot(cancellations,color = "blue")
    ax1.plot(weather_cancellations,color = "gold")
    ax1.set_ylabel("Cancellations")
    ax1.set_xlabel("Month")
    ax1.set_title("By Month: Total Cancellations vs Cancellations due to Weather\nBlue: Total  |  Gold: Weather Cancellations")
    fig1.savefig("Visualization1.png")
    
    #Lets get some data to build figure 2
    aircraft_delays, names, total_flights,delay_late_aircraft,delays_carrier = tot_delays(data)
    percentageNAS = get_percent(aircraft_delays,total_flights)
    percentageLateA = get_percent(delay_late_aircraft, total_flights)
    percentageCarrier = get_percent(delays_carrier, total_flights)

    #second figure construction
    fig2, ax2 = plt.subplots()
    ax2.bar(names,percentageNAS,width = .9, color = "red")
    ax2.bar(names,percentageLateA, width = .6, color = "orange")
    ax2.bar(names,percentageCarrier, width = .4, color = "green")
    ax2.set_ylabel("Flights Delayed")
    ax2.set_xlabel("Airport Code - Top 12 Airports")
    ax2.set_title("Percentage of Flights Delayed Due to NAS, Late Aircraft, and Carriers \n Red: NAS, Orange: Late Aircraft, Green: Carrier")
    fig2.savefig("visualization2.png")
    
main()
