# -----------------------------------------------------
# CSCI 127, Lab 8                                     |
# June 25, 2019                                       |
# Timothy Bender                                      |
# -----------------------------------------------------

class Queue:

    def __init__(self,number):
        self.contents = ""
    
    def enqueue(self,number):
        self.contents += (str(number) + " ")
    
    def dequeue(self):
        item_dequed = self.contents[0]
        self.contents = self.contents[2:]
        return item_dequed
    
    def is_empty(self):
        if len(self.contents) == 0:
            return True
        
    def __str__(self):
        return(str("Contents: " + self.contents))

    def __iadd__(self,number):
        self.contents = self.contents[0:-1] + " " + (str(number)).strip()
        return self
    

# -----------------------------------------------------

def main():
    numbers = Queue("Numbers")

    print("Enqueue 1, 2, 3, 4, 5")
    print("---------------------")
    for number in range(1, 6):
        numbers.enqueue(number)
        print(numbers)

    print("\nDequeue one item")
    print("----------------")
    numbers.dequeue()
    print(numbers)

    print("\nDeque all items")
    print("---------------")
    while not numbers.is_empty():
        print("Item dequeued:", numbers.dequeue())
        print(numbers)

    # Enqueue 10, 11, 12, 13, 14
    for number in range(10, 15):
        numbers.enqueue(number)

    # Enqueue 15
    numbers += 15

    print("\n10, 11, 12, 13, 14, 15 enqueued")
    print("-------------------------------")
    print(numbers)

# -----------------------------------------------------

main()
