import numpy as np
import matplotlib.pyplot as plt

#fixing random state for reproducibility
np.random.seed(19680801)

mu = 100 #mean
sigma = 15 #standard deviation
#np.random.randn:
#Return a sample ( or samples) from the "standard normal" distribution
x = mu + sigma * np.random.randn(10000) #10000 element numpy array

#plot a histogram of data in np array x and "bins". 10 bins is the default
#normalized with "density"

plt.hist(x,50, density=1)
plt.show()
