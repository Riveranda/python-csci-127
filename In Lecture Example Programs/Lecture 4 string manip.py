def get_length(string):
    #use built in string length definition
    return len(string)



def get_length_iterative(string):
    #use iteration
    count = 0 
    for ch in string:
        count = count + 1
    return count


def get_length_recursive(string):
    #use recursion
    #base case
    n = 0
    if len(string) == 1:
        return 0
    if string =="z":
        return 1
    else:
        return string[0] + get_length_recursive(string[1:])
        



print(get_length("This string is 28 characters"))
print(get_length_iterative("This string is 28 characters"))
print(get_length_recursive("This string is 28 characters"))



