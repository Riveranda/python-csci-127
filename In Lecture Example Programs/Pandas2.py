import pandas as pd

"""Stitching existing variables into a dataset and putting it into a file"""
years  = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018]
total_snowfall = [253, 304, 388, 265, 283, 209, 194, 271, 177]
largest_snowfall = [19, 16, 19, 25, 20, 14, 13, 20, 15]
BridgerDataSet = list(zip(years, total_snowfall, largest_snowfall))
bb_data = pd.DataFrame(data=BridgerDataSet, columns=["Year", "Total", "Largest"])
bb_data.to_csv('bridger_data.csv')

#if you don't want an index
bb_data.to_csv('bridger_data.csv', index=False)
plt.plot(bb_data.Year, bb_data.Total
plt.show()
