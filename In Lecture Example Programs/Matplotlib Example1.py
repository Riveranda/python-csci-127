import matplotlib
import numpy as np

import matplotlib.pyplot as plt
"""SUBPLOTS"""

#subplots: plt.subplot(rows, columns, figurenumber
t = np.linspace(0,5,50)

plt.subplot(2, 1, 1)
plt.plot(t, t**2, "bo")
#change to sublpot 2
plt.subplot(2,1,2)
plt.plot(t,np.cos(2*np.pi*t), "r--")

plt.show()



#our second plot: plot y=x, y=x^2, y=x^3 using numpy arrays
t = np.linspace(0,10,50) #50 evenly spaced  data points between 0 and 10
#you can pass multiple data values in. 2 plots in one
plt.plot(t,t,"g-",t, t**2, "r--",t,t**3,"p--") #y=x line. green, - makes it a line
                               #y=x^2 is red as a line
plt.show()

#create a plot.
#x,y, color/lin3 style
#we can pass in lists
#will convert it to a numpy array
plt.plot([1,2,3,4],[1,4, 9, 16],"ro") #r means red, o means circle
#change the y label
plt.ylabel("some numbers")

plt.show()



