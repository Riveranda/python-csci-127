import pandas as pd
#import os
#os.getcwd()
#os.chdir()
data = pd.read_csv('imbd_1000.csv')
print(data.head()) #will print first 5 rows
print(data.tail()) #will print last 5 rows
data.dtypes
data.shape #will return number of rows and number of columbs
rows = data.shape[0] #will assign rows to 979

data.describe() #produce numeric summary of data. only gives info on numeric columns
data.title.head()

data['actors_list'].head() #printing a column
data.actors_list.head() #printing a column

data.star_rating.tail() #give the lowest score

data.duration.mean() #average duration of the movies
data.duration.sum() #sum of duration

"""Selecting columns"""
data[['star_rating','title','genre']].head()


"""Filtering"""
#movies with a rating greater than 8.7
data[data.star_rating > 8.7]

#movies with a rating greater than 8.7 and viewing the title
data[data.star_rating > 8.7]['title']

#return an array of the unique values in a column
data.genre.unique()

#just get history movies
data[data.genre == "History"].title


"""multiple filtering"""
#syntax is data[(condition1) & (condition2)]
data[(data.star_rating > 8.7) & (data.genre == "Action")]

number_history_movies = data[data.genre == "History"].shape[0]
print(number_history_movies)

"""Counting how many things are in your data set"""
data.genre.value_counts()
#with percentages
data.genre.value_counts(normalize=True)



##"""Printing a plot of data"""
##duration_data = data.duration
##rating_data = data.star_rating
##import matplotlib.pyplot as plt
##plt.plot(rating_data, duration_data, "go")
##plt.show()

"""Practice stuff"""
average_duration = data.duration.mean()
two_columns = data[["star_rating","duration"]]
print(two_columns)
longest_duration = (data.duration.nlargest(10))
print(longest_duration)


data.groupby('genre')
print(data.groupby('genre').mean()['star_rating'].sort_values(ascending=False))
#print(data.groupby('genre').describe())



