#write a program that implements a Ceasar cipher on
#lowercase letters.
import string

all_letters = string.ascii_lowercase

def ceasar_cipher(word):
    key = 3
    result = ""
    for letter in (word):
        if letter != " ":
            index = all_letters.index(letter)
            length = len(all_letters)
            new_letter = all_letters[(index + key) % length]
            result = result + new_letter
        else:
            result = result = " "
    result = word
    return(result)

test1 = "montana state university"
test2  = "we went to rendezvous for lunch"
print(ceasar_cipher(test1))
print(ceasar_cipher(test2))
