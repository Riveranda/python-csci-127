# --------------------------------------
# CSCI 127: Joy and Beauty of Data
# MatPlotLib Introductory Example
# --------------------------------------

import matplotlib.pyplot as plt
import numpy as np
import math

# --------------------------------------

def plot_line(x1, y1, x2, y2):
    """Plot a line using the specified points."""
    x = [x1, x2]
    y = [y1, y2]
    plt.plot(x, y, "m--", markersize = 5)


# --------------------------------------

def plot_sine_wave(start_x, stop_x, amplitude):
    """Plot a sine wave."""
    x_array = np.linspace(start_x, stop_x, 1000)
    y_array = amplitude * np.sin(x_array)
    plt.plot(x_array, y_array)

#---------------------------------------
def plot_new_func(exponent):
    t = np.linspace(0,10)
    plt.plot(t,t**exponent, "g--")
    
    

# --------------------------------------

def main(graph_min, graph_max):
    plt.ylabel("Height")
    plt.xlabel("Length")

    plt.xlim(graph_min, graph_max) #fixing matlpotlibs minimum and maximum axis values
    plt.ylim(graph_min, graph_max)
    
    #plot gold line from -100 to 100 on x and y axis
    plot_line(graph_min, graph_min, graph_max, graph_max)

    #creates an array containing the graph's minimum and maximum X values
    x_array = np.array([graph_min, graph_max])
    
    #creates an array containing the graph's minimum and maximimum Y values
    y_array = np.array([graph_max, graph_min])
    
    #plots a blue line on domain x_array, and y_array
    plt.plot(x_array, y_array, "m--", markersize=5)
    
    #calls the plot_sine_wave function, and cuts the domain in half. 
    plot_sine_wave(graph_min // 2, graph_max // 2, graph_max // 4)

    plot_new_func(graph_max/50)

    #show the plot
    plt.show()

# --------------------------------------

main(-100, 100)
