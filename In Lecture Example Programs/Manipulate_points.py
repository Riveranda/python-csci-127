import point
p = point.Point(7, 6)
q = point.Point(-1, 1.5)
r = point.Point()
print(p.halfway(q))
print(q.halfway(p))

def distance_between_points(p1, p2):
    p1_x = p1.getX()
    p2_x = p2.getX()
    p1_y = p1.getY()
    p2_y = p1.getY()
    return ((p1_x - p2_x)**2 +(p1_y - p2_y)**2)**.5
