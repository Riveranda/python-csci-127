import turtle

def drawSquare(t, sz):
    """Make turtle t draw a square of with side sz."""
    for i in range(4):
        t.forward(sz)
        t.left(90)
wn = turtle.Screen()
alex = turtle.Turtle()
alex.penup()
alex.goto(-200,0)
alex.pendown()
y = -200
# Set up the window and its attributes
wn.bgcolor("lightgreen")
for x in range(5):# create alex
    y = y + 75
    drawSquare(alex, 50)
    alex.penup()
    alex.goto(y,0)# Call the function to draw the square passing the actual turtle and the actual side size
    alex.pendown()
wn.exitonclick()
