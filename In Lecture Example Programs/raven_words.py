f = open('raven.txt', 'r')

word_dict = {}
for line in f:
    #print(line.split())
    for word in line.split():
        # remove punctuation
        word = word.replace('_', '').replace('"', '').replace(',', '').replace('.', '')
        word = word.replace('-', '').replace('?', '').replace('!', '').replace("'", "")
        word = word.replace('(', '').replace(')', '').replace(':', '').replace('[', '')
        word = word.replace(']', '').replace(';', '')
        word = word.lower()
        # at this point, we have word
        
        if word_dict.get(word, -1) == -1:
            word_dict[word] = 1
        else:
            word_dict[word] += 1
keylist = list(word_dict.keys())
keylist.sort()
keylist = keylist[1:]
for i in keylist:
    print(str(i) + " "+ str(word_dict.get(i)))
        

f.close()


