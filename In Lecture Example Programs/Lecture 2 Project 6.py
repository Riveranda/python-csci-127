#make a number guessing game between 1 - 10. Print out when input is correct

import random
number = int(10*random.random()) + 1
number_of_guesses = 0
right_guess = False
while  right_guess == False:
    guess = int(input("Please input your guess between 1 and 10 "))
    number_of_guesses = number_of_guesses + 1
    if guess == number:
        number_of_guesses = str(number_of_guesses)
        print("Your guess is correct")
        print ("It took you "+ number_of_guesses + " times to guess correctly")
        right_guess = True
    else:
        print("Incorrect")
    
