#Linear Regression Modeling
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import numpy as np
train = pd.read_csv("train.csv")
#plt.plot(train["1stFlrSF"], train['SalePrice'], "bo")
#plt.show()
reg_object = LinearRegression()
#data must be a 2d numpy array where each item is in its own array
#X = np.array(train['1stFlrSF']).reshape(-1,1)
Y = train["SalePrice"]
#reg_object.fit(X,Y)
#reg_object.predict(np.array(1000).reshape(-1,1))
#score the model
#reg_object.score(X,Y)

X = np.array(list(zip(train["1stFlrSF"],train["LotArea"],train["GrLivArea"],train["YearBuilt"],train["YearRemodAdd"],train["TotalBsmtSF"],train['YrSold']))).reshape(-1,7)
#reg_object.fit(X,Y)
#predictions = reg_object.predict(X)

from sklearn.tree import DecisionTreeRegressor
dectree_object = DecisionTreeRegressor()
dectree_object.fit(X,Y)
predictions = dectree_object.predict(X)

from sklearn.metrics import mean_squared_log_error
score = np.sqrt(mean_squared_log_error(Y, predictions))
print(score)
