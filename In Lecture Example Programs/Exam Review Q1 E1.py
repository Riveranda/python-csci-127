def remove_e(string):
    return string.replace("e","")
#Using string function

print(remove_e("Hello"))

def remove_e_recursion(string):
#Using recursion 
    if len(string) == 0:
        return ""
    if string[0] != "e":
        return string[0] + remove_e_recursion(string[1:])
    else:
        return "" + remove_e_recursion(string[1:])
    
 

print(remove_e_recursion("Hello")) 
