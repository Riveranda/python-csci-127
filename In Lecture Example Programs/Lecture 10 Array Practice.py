# Create a one-dimensional array called v with 10 integers.
# Each integer should be a random number between 1 and 100.
import random
import numpy as np
v = np.random.randint(0,100,10)
print(v)

# Create a new array which consists of the odd indices of
# previously created array v.
b = v[np.array([1,3,5,7,9])]
print(b)

# Create a new array in backwards ordering from v.
c = v[ : :-1]
print(c)

# Create a two-dimensional array called m with 25 integers
# in a 5 by 5 matrix. Each integer should be a random number
# between 1 and 100.
m = np.random.randint(1,100,(5,5))
print(m)
print(" ")

# Create a new array from m, in which the elements of each row
# are in reverse order.
d = m[:,::-1]
print(d)
print(" ")

# Create another array from m, where the rows are in reverse order.
e = m[::-1]
print(e)
print(" ")

# Create another array from m, where columns and rows are in reverse order.
f = m[::-1,::-1]
print(f)

# Create another array from m, where the first and last row and
# the first and last column are cut off.
print("")
g = m[1:-1,1:-1]
print(g)
