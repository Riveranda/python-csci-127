import turtle
# The missing function goes here but write it below.
drawing_turtle = turtle.Turtle()
drawing_turtle.width(3)
drawing_turtle.hideturtle()
number_segments = int(input("Enter number of segments: "))
# Assume the user will enter an integer >= 1
segment_length = int(input("Enter length of a segment: "))
# Assume the user will enter an integer >= 10

def bobcat_line(turtle, number, length): 
    for i in range(number):
        if (i % 2) == 0:
            turtle.pencolor("gold")
        else:
            turtle.pencolor("blue")
        turtle.forward(length)


bobcat_line(drawing_turtle, number_segments, segment_length)
        
