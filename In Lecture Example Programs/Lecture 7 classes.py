class Point:

    def __init__(self, initX, initY):
        self.x = initX
        self.y = initY
        
    #magic method for overriding default print
    def __str__(self):
        """Override print function to print (x,y)"""
        return "(" + str(self.x) + "," + str(self.y) + ")"
    #magic method for using the + operator

    def __add__(self,other):
        pass

    #magic method for overriding default ==
    def __eq__(self,other):
        #return true if x and y are equal
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False

p = Point(2, 3)
r = Point(1, 1)
p + r
print(p)
print(r)
print(p == r)
