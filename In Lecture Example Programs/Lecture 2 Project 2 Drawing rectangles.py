import turtle

xs = [48, 177, 200, 240, 160, 260, 220]
window = turtle.Screen()    #create turtle window
window.bgcolor("lightgreen")    #Set window color
alex = turtle.Turtle()      #Create turtle
alex.penup()
alex.goto(-180,-200)
alex.pendown()
def draw_bar(height, t):
    #Get turtle t to draw a rectangle of height height#
    t.left(90)
    t.forward(height)
    t.right(90)
    t.forward(40)
    t.right(90)
    t.forward(height)
    t.left(90)

for h in xs:
    draw_bar(h,alex)
