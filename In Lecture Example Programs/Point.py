class Point:
        "””A point class for representing and manipulating points.”””
	def __init__(self, initX,  initY):
	"””Create a new point at the specified coordinates.”””
		self.x = initX
		self.y = initY
		
	def getX(self):
		return self.x
	    
	def getY(self):
		return self.y
	    
	def distanceFromOrigin(self):
	"””Get distance from Point object to origin”””
	#formula for distance to origin: sqrt(x1^2 + y1^2)
	return (self.x**2 + self.y**2)**(1/2)

        def halfway(self,other_points):
            """Return a point that is halfway between this point and a target point object"""
        new_x = (self.x + otherpoint.getX())/2
        new_y = (self.y + otherpoint.getY())/2

