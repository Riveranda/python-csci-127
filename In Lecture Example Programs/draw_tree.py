import turtle

def tree(branchLen,t):
    #base case: branchLen is 5 (or less)
        #do nothing
    
    if branchLen > 5:
        t.forward(branchLen)
        t.right(20)
        #moves toward base case by inputting branchLen - 15
        tree(branchLen-15,t)
        t.left(40)
        #moves toward base case by inputitng branchLen - 15
        tree(branchLen-15,t)
        t.right(20)
        t.backward(branchLen)

def main():
    t = turtle.Turtle() # create the turtle 
    myWin = turtle.Screen()     #create the screen
    t.up()                  #lifts up pen 
    t.goto(0, -100)
    #moves the turtle to (0 , -100)
    t.left(90)
    t.down()                #puts pen down    
    t.color("green")        #changes the turtle color to green
    tree(75,t)              #calls tree with arguements 75, and our turtle
    myWin.exitonclick()

main()
